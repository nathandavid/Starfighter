package managers
{
	import com.leebrimelow.starling.StarlingPool;
	
	import objects.Alien;
	import states.Play;
	
	import starling.core.Starling;
	
	public class AlienManager {
		
		private var play:Play;
		public var aliens:Array;
		private var pool:StarlingPool;
		public var count:int = 0;
		
		public function AlienManager(play:Play) {
			this.play = play;
			aliens = new Array();
			pool = new StarlingPool(Alien, 20);
		}
		
		public function update():void {
			if (Math.random() * 10 > 9.9) {
				spawn();
			}
			
			var a:Alien;
			var len:int = aliens.length;
			
			for (var i:int = len-1; i >= 0; i--) {
				a = aliens[i];
				a.x -= 8;
				
				if (a.x < 32) {
					destroyAlien(a);
				}
			}
		}
		
		private function spawn():void {
			var a:Alien = pool.getSprite() as Alien;
			Starling.juggler.add(a);
			aliens.push(a);
			a.y = Math.random() * 400 - 32;
			a.x = 832;
			play.addChild(a);
		}
		
		public function destroyAlien(a:Alien):void {
			var len:uint = aliens.length;
			
			for (var i:uint = 0; i < len; i++) {
				if(a == aliens[i]) {
					aliens.splice(i, 1);
					Starling.juggler.remove(a);
					a.removeFromParent(true);
					pool.returnSprite(a);
				}
			}
		}
		
		public function destroy():void {
			pool.destroy();
			pool = null;
			aliens = null;
		}
	}
}