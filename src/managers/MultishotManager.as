package managers
{
	import com.leebrimelow.starling.StarlingPool;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import objects.Bullet;
	import objects.Score;
	
	import states.Play;
	
	public class MultishotManager {
		
		private var play:Play;
		public var bullets:Array;
		private var pool:StarlingPool;
		private var fireDelay:Timer;
		private var canFire:Boolean = true;
		
		public static var currentLevel:uint;
		
		public function MultishotManager(play:Play) {
			this.play = play;
			bullets = new Array();
			pool = new StarlingPool(Bullet, 12);
			
			currentLevel = 0;
		}
		
		public function update():void {
			
			fireDelay = new Timer(500, 1);
			fireDelay.addEventListener(TimerEvent.TIMER, onFireTimer);
			
			var b:Bullet;
			var len:int = bullets.length;
			
			for (var i:int = len-1; i >= 0; i--) {
				b = bullets[i];
				b.x += 25;
				
				if (b.x > 800) {
					destroyBullet(b);
				}
			}
			
			if (play.fireMultishot && canFire && Score.getEnergy() > 0 && currentLevel > 0) {
				fire()
				canFire = false;
				fireDelay.start();
				
				//ENERGY COST
				switch (currentLevel) {
					case 1 : Score.setEnergy(-3);
						break;
					case 2 : Score.setEnergy(-6);
						break;
					case 3 : Score.setEnergy(-9);
						break;
				}
			}
		}
		
		/* FIRE TIMER EVENT */
		private function onFireTimer(e:TimerEvent):void {
			canFire = true;
		}
		
		private function fire():void {
			var a:Bullet = pool.getSprite() as Bullet;
			var b:Bullet = pool.getSprite() as Bullet;
			var c:Bullet = pool.getSprite() as Bullet;
			
			play.addChild(a);
			a.x = play.hero.x + 20;
			a.y = play.hero.y + 10;
			
			play.addChild(b);
			b.x = play.hero.x + 20;
			b.y = play.hero.y;
			
			play.addChild(c);
			c.x = play.hero.x + 20;
			c.y = play.hero.y - 10;
			
			bullets.push(a);
			bullets.push(b);
			bullets.push(c);
			
			//Assets.shoot.play();
		}
		
		public function destroyBullet(b:Bullet):void {
			var len:int = bullets.length;
			
			for (var i:int = 0; i < len; i++) {
				if (bullets[i] == b) {
					bullets.splice(i, 1);
					b.removeFromParent(true);
					pool.returnSprite(b);
				}
			}
		}
		
		public function destroy():void {
			pool.destroy();
			pool = null;
			bullets = null;
		}
	}
}