package states
{
	import core.Assets;
	import core.Game;
	import interfaces.IState;
	import objects.NativeCursor;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.PDParticleSystem;
	import starling.text.TextField;
	
	public class Menu extends Sprite implements IState {
		
		private var game:Game;
		private var logo:Image;
		private var play:Button;
		private var cursor:NativeCursor;
		private var version:TextField;
		private var space:PDParticleSystem;
		
		public function Menu(game:Game) {
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
			
			space = new PDParticleSystem(XML(new Assets.spaceXML()),
				Assets.ta.getTexture("particle"));
			Starling.juggler.add(space);
			addChild(space);
			space.start();
		}
		
		private function init(e:Event):void {
			
			cursor = new NativeCursor();
			
			version = new TextField(100, 30, "Beta 0.1.3", "Orator Std", 12, 0xFF0000);
			version.x = 700;
			version.y = 0;
			addChild(version);
			
			logo = new Image(Assets.ta.getTexture("logo"));
			logo.pivotX = logo.width * 0.5;
			logo.x = 420;
			logo.y = 100;
			addChild(logo);
			
			play = new Button(Assets.ta.getTexture("play"));
			play.addEventListener(Event.TRIGGERED, onPlay);
			play.pivotX = play.width * 0.5;
			play.x = 400;
			play.y = 250;
			addChild(play);
		}
		
		private function onPlay(e:Event):void {
			play.removeEventListener(Event.TRIGGERED, onPlay);
			game.changeState(Game.PLAY_STATE);
		}
		
		public function update():void {
			space.emitterX = 400;
			space.emitterY = 200;
			space.emitAngleVariance = 360;
		}
		
		public function destroy():void {
			logo.removeFromParent(true);
			logo = null;
			play.removeFromParent(true);
			play = null;
			removeFromParent(true);
		}
	}
}