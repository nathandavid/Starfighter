package states
{
	import core.Assets;
	import core.Game;
	
	import interfaces.IState;
	
	import objects.NativeCursor;
	
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class GameOver extends Sprite implements IState {
		
		private var game:Game;
		private var overText:TextField;
		private var tryAgain:Button;
		private var back:Button;
		private var cursor:NativeCursor;
		
		public function GameOver(game:Game) {
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void {
			cursor = new NativeCursor();
			
			overText = new TextField(300, 100, "Game Over", "Orator Std", 42, 0xFFFFFF);
			overText.x = 250;
			overText.y = 100;
			addChild(overText);
			
			tryAgain = new Button(Assets.ta.getTexture("again"));
			tryAgain.addEventListener(Event.TRIGGERED, onAgain);
			tryAgain.pivotX = tryAgain.width * 0.5;
			tryAgain.x = 400;
			tryAgain.y = 250;
			addChild(tryAgain);
			
			back = new Button(Assets.ta.getTexture("back"));
			back.addEventListener(Event.TRIGGERED, onBack);
			back.pivotX = back.width * 0.5;
			back.x = 400;
			back.y = 300;
			addChild(back);
		}
		
		private function onAgain(e:Event):void {
			tryAgain.removeEventListener(Event.TRIGGERED, onAgain);
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onBack(e:Event):void {
			back.removeEventListener(Event.TRIGGERED, onBack);
			game.changeState(Game.MENU_STATE);
		}
		
		public function update():void {
		}
		
		public function destroy():void {
			removeFromParent(true);
		}
	}
}