package objects
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.ui.Mouse;
	import flash.ui.MouseCursorData;

	public class NativeCursor extends Sprite{
		
		public var newCursor:Cursor;
		private var cursorBitmapData:BitmapData;
		private var cursorData:MouseCursorData;
		private var cursorVector:Vector.<BitmapData>;
		
		public function NativeCursor() {
			if(Mouse.supportsNativeCursor){
				setupMouseCursor();
			}
		}
		
		private function setupMouseCursor():void {
			newCursor = new Cursor();
			cursorBitmapData = new BitmapData(24, 32, true, 0x000000);
			cursorBitmapData.draw(newCursor);
			
			cursorVector = new Vector.<BitmapData>();
			cursorVector[0] = cursorBitmapData;
			
			cursorData = new MouseCursorData();
			cursorData.hotSpot = new Point(0, 0);
			cursorData.data = cursorVector;
			
			Mouse.registerCursor("newCursor", cursorData);
			Mouse.cursor = "newCursor";
		}
	}
}