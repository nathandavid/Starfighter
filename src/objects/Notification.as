package com.natedavidrivera.app
{	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class Notification extends NotificationBase {
		
		private var _count:int;
		private var _notif:Array;
		private var _delay:int;
		private var _delayTimer:Timer;
		
		public function Notification() {
			_notif = [];
			_count = 0;
		}
		
		public function set tip(value:String):void {
			this.tfTip.visible = true;
			this.tfTip.text = value;
		}
		
		public function close():void {
			this.visible = false;
		}
		
		/* 
			Pushes a string value into the array
		*/
		public function add(value:String):void {
			_notif.push(value);
		}
		
		/*  
			Calls a specific notification by index
		*/
		public function call(index:int):void {
			this.tfTip.text = _notif[index];
		}
		
		/*
		 	Recieves the delay and starts the timer
		*/
		public function play(delay:int):void {
			alpha = 0;
			_delay = delay;
			_delayTimer = new Timer(delay*1000, 1);
			_delayTimer.addEventListener(TimerEvent.TIMER, onTimer);
			_delayTimer.start();
		}
		
		/*
			Plays all notifications in the array, with a specificed delay in seconds
		*/
		private function onTimer(e:TimerEvent):void {
			fade("IN");
			if (_count < _notif.length) {
				this.tfTip.text = _notif[_count];
				_count++;
				play(_delay);
			}
			
			if (_count == _notif.length) {
				_delayTimer.removeEventListener(TimerEvent.TIMER, onTimer);
			}
		}
		
		/*
			Calls the enterframe events to intiate fade animations
		*/
		private function fade(value:String):void {
			if (value == "IN") {
				this.addEventListener(Event.ENTER_FRAME, onFadeIn);
			} else if (value == "OUT") {
				this.addEventListener(Event.ENTER_FRAME, onFadeOut);
			}
		}
		
		private function onFadeIn(e:Event):void {
			this.alpha += .01;
		}
		
		private function onFadeOut(e:Event):void {
			this.alpha -= .01;
		}
	}
}