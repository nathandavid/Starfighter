package events
{
	import starling.events.Event;
	
	public class LevelEvent extends Event {
		
		public static const LEVEL_UP:String = "level up";
		
		public function LevelEvent(type:String, bubbles:Boolean=false, data:Object=null) {
			super(type, bubbles, data);
		}
	}
}